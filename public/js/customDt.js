class customDt {
  constructor(props = {}, search = { isSearch: false, column: 1 }, dataFilter = [], dtExport = { title: "Report" }) {
    this.props = props;
    this.search = search;
    this.dataFilter = dataFilter;
    this.export = dtExport;
    this.dtInstance = null;
    this.status = [
      "Inactive",
      "Active",
      "Progress",
      "Submited",
      "Approved",
      "Success",
      "Failed",
      "Cancelled",
    ]
  }

  init() {
    this.dtInstance = $('#dataTable').DataTable({
      orderCellsTop: true,
      fixedHeader: true,
      processing: true,
      serverSide: true,
      ...this.props,
    });

    const self = this;
    // Perform a search in the second column (index 1)
    if (self.search.isSearch) {
      $('#searchInput').on('keyup', function () {
        this.dtInstance.column(self.search.column).search($('#searchInput').val()).draw();
      });
    }

    // this.dtInstance.column(9).search("Single").draw();


    // Hook export buttons
    const documentTitle = this.export.title;
    new $.fn.dataTable.Buttons(this.dtInstance, {
      buttons: [
        {
          extend: 'excelHtml5',
          title: documentTitle
        },
        {
          extend: 'csvHtml5',
          title: documentTitle
        },
        {
          extend: 'pdfHtml5',
          title: documentTitle
        }
      ]
    }).container().appendTo($('#kt_datatable_example_buttons'));

    // Hook dropdown menu click event to datatable export buttons
    const exportButtons = document.querySelectorAll('[data-kt-export]');
    exportButtons.forEach(exportButton => {
      exportButton.addEventListener('click', function (e) {
        e.preventDefault();

        // Get clicked export value
        const exportValue = e.target.getAttribute('data-kt-export');
        const target = document.querySelector('.dt-buttons .buttons-' + exportValue);

        // Trigger click event on hidden datatable export buttons
        target.click();
      });
    });
  }

  // Method to update filters
  updateFilters(dataFilter) {
    this.dataFilter = dataFilter;

    if (this.dataFilter.length > 0) {
      this.dataFilter.forEach((item) => {
        if (item.index != 10) {
          console.log("!= 10 : " + item.value)
          this.dtInstance.column(item.index).search(item.value).draw();
        } else {
          var table = this.dtInstance;
          const status = this.status;
          item.value.forEach(function(searchVal){
            console.log(searchVal)
           table.columns(item.index).every(function(){
              var column = this;
              column.search(status[searchVal]).draw();
            })
          })
        }
      });
    }
  }

  // Method to reload data
  reloadData() {
    this.dtInstance.ajax.reload();
  }


}