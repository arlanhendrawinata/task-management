const columns = [
  {
    "data": 'DT_RowIndex',
    "name": 'DT_RowIndex',
    "orderable": false
  },
  {
    "data": "task",
    "name": "task"
  },
  {
    "data": "clients.companyclients.name",
    "name": "clients.companyclients.name"
  },
  // { "data": "clients.nama_client", "name": "clients.nama_client"  },
  // { "data": "users.nama", "name": "users.nama" },
  // { "data": "tgl_input", "name": "tgl_input" },
  { "data": "divisions.nama_divisi", "name": "divisions.nama_divisi", "visible": false },
  {
    "data": "pic",
    "name": "pic"
  },
  {
    "data": "task_target",
    "name": "task_target"
  },
  {
    "data": "tgl_mulai",
    "name": "tgl_mulai"
  },
  {
    "data": "tgl_selesai",
    "name": "tgl_selesai"
  },
  {
    "data": "task_result",
    "name": "task_result"
  },
  // { "data": "task_detail", "name": "task_detail" },
  { "data": "type", "name": "type", "visible": false },
  {
    "data": "task_status",
    "name": "task_status"
  },
  // Add more columns as needed
]