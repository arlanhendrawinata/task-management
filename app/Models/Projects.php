<?php

namespace App\Models;

use App\Enums\PriorityEnum;
use App\Enums\ProTaskStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    use HasFactory;

    protected $fillable = [];

    protected $cast = [
        'priority' => PriorityEnum::class,
        'status' => ProTaskStatusEnum::class
    ];
}
