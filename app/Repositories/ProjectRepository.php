<?php 

namespace App\Repositories;

use App\Models\Project;

class ProjectRepository {
    function __construct(protected Project $model)
    {}

    public function all()
    {
      return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function with(array $model){
      return $this->model->query()
      ->with($model);
    }

    public function whereType(array $type){
      return $this->model->query()->where('type', $type);
    }

    public function getStatusCount(array $status){
      return $this->model->query()
      ->where('status', $status)
      ->count();
    }

    /**
     * Create an item
     * @param array|mixed $data
     * @return Model|null
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * Update a item
     * @param int|mixed $id
     * @param array|mixed $data
     * @return bool|mixed
     */
    public function update($id, array $data)
    {
        return $this->model->findOrFail($id)->update($data);
    }

    public function query(){
      return $this->model->query();
    }

    /**
     * destroy many item with primary key
     * @param int|Model $id
     */
    public function destroy(array $id)
    {
        return $this->model->destroy($id);
    }

    /**
     * delete item
     * @param Model|int $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }
}

?>