<?php

namespace App\Repositories;

use App\Models\Company;

class CompanyRepository{
  public function __construct(protected Company $model)
  {}

  public function selectColumns(array $columns){
    return $this->model->query()->get($columns);
  }
}