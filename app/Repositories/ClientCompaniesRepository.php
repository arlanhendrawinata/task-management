<?php 
namespace App\Repositories;

use App\Models\Companyclient;

class ClientCompaniesRepository{
  public function __construct(protected Companyclient $model)
  {}

  public function selectColumns(array $columns){
    return $this->model->query()->get($columns);
  }
}