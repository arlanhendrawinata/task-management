<?php 
namespace App\Repositories;

interface BaseRepositoryInterface{
   /**
     * Fin an item by id
     * @param mixed $id
     * @return Model|null
     */
    public function find($id);

    /**
     * find Or Fail
     * @param $id
     * @return mixed
     */
    public function findOrFail($id);

    /**
     * Return all items
     * @return Collection|null
     */
    public function all();

    /**
     * Create an item
     * @param array|mixed $data
     * @return Model|null
     */
    public function create($data);

    /**
     * Update a item
     * @param int|mixed $id
     * @param array|mixed $data
     * @return bool|mixed
     */
    public function update($id, array $data);

    /**
     * destroy many item with primary key
     * @param int|Model $id
     */
    public function destroy(array $id);

    /**
     * delete item
     * @param Model|int $id
     * @return mixed
     */
    public function delete($id);
} 


?>