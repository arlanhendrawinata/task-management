<?php

namespace App\Enums;

enum ProTaskStatusEnum: string {
  case Inactive = 'inactive';
  case Active = 'active';
  case Progress = 'progress';
  case Submitted = 'submitted';
  case InReview = 'in review';
  case Approved = 'approved';
  case Revision = 'revision';
  case Success = 'success';
  case Failed = 'failed';
  case Cancelled = 'cancelled';
}