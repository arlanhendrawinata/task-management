<?php

use App\Models\Division;
use App\Models\UserDetail;

function getDay($date)
{
  $dateFormat = date("d/m/Y", strtotime($date));
  $day = \Carbon\Carbon::createFromFormat('d/m/Y', $dateFormat)->format('d');
  return $day;
}


function monthList()
{
  $monthList = [
    [
      'id' => 1,
      'nama' => 'January'
    ],
    [
      'id' => 2,
      'nama' => 'February'
    ],
    [
      'id' => 3,
      'nama' => 'March'
    ],
    [
      'id' => 4,
      'nama' => 'April'
    ],
    [
      'id' => 5,
      'nama' => 'May'
    ],
    [
      'id' => 6,
      'nama' => 'June'
    ],
    [
      'id' => 7,
      'nama' => 'July'
    ],
    [
      'id' => 8,
      'nama' => 'August'
    ],
    [
      'id' => 9,
      'nama' => 'September'
    ],
    [
      'id' => 10,
      'nama' => 'October'
    ],
    [
      'id' => 11,
      'nama' => 'November'
    ],
    [
      'id' => 12,
      'nama' => 'December'
    ]
  ];
  // $monthList = [
  //   'January' => 1, 'February' => 2, 'March' => 3, 'April' => 4, 'May' => 5, 'June' => 6, 'July' => 7, 'August' => 8, 'September' => 9, 'October' => 10, 'November' => 11, 'December' => 12
  // ];
  return $monthList;
}

function getMonth($val)
{
  $dateFormat = date("d/m/Y", strtotime($val));
  $month = \Carbon\Carbon::createFromFormat('d/m/Y', $dateFormat)->format('m');
  return $month;
}

function getMonthName($array)
{
  // $countArray = count($array);
  $array2 = monthList();
  for ($i = 0; $i < count($array2); $i++) {
    // echo $array[$i];
    for ($j = 0; $j < count($array); $j++) {
      if ($array[$j]['month'] == monthList()[$i]['id']) {
        $real_month[] = [
          'id' => $array[$j]['month'],
          'nama' => monthList()[$i]['nama'],
          'year' => $array[$j]['year'],
        ];
      }
    }
  }
  return $real_month;
}

function initial($str)
{
  $nameparts = explode(" ", $str);
  $ret = '';
  foreach ($nameparts as $word)
    $ret .= strtoupper($word[0]);
  return $ret;
}

function firstname($str)
{
  $nameparts = explode(" ", $str);
  return $nameparts[0];
}

function countMemberTeam($id_divisi)
{
  $count = UserDetail::where('divisi_id', $id_divisi)->count();
  return $count;
}

function read_status()
{
  $status = [
    0 => "Inactive",
    1 => "Active",
    2 => "Progress",
    3 => "Submited",
    4 => "Approved",
    5 => "Success",
    6 => "Failed",
    7 => "Cancelled",
  ];

  return $status;
}

function status_color()
{
  $color = [
    0 => "badge-light-secondary",
    1 => "badge-light-success",
      2 => "badge-light-warning",
      3 => "badge-light-warning",
      4 => "badge-light-info",
      5 => "badge-light-primary",
      6 => "badge-light-danger",
      7 => "badge-light-danger",
  ];
  return $color;
}

function statusClasses(){
  $statusClasses = [
    1 => 'badge-light-success',
    2 => 'yellow',
    3 => 'orange',
    4 => 'teal',
    5 => 'success',
    6 => 'danger',
    7 => 'red',
    0 => 'grey'
];
return $statusClasses;
}
