<?php
namespace App\Services;

use App\Repositories\CompanyRepository;

class CompanyService {
  public function __construct(protected CompanyRepository $repo)
  {}

  public function getSelectedColumns(){
    return $this->repo->selectColumns(['id', 'nama_perusahaan']);
  }
}