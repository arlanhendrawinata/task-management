<?php

namespace App\Services;

use App\Repositories\ProjectRepository;

class ProjectService
{
  function __construct(protected ProjectRepository $repo)
  {
  }

  public function getWith_IdStatus()
  {
    return $this->repo->with(['companies', 'clients.companyclients', 'divisions', 'users'])
      ->orderBy('id', 'desc')
      ->orderBy('status', 'desc')
      ->get();
  }

  public function collectCountStatus()
  {
    $status = ["active", "progress", "submit", "verif", "success"];
    $collection = collect();
    for ($i = 0; $i < 5; $i++) {
      $collection->put($status[$i], $this->repo->whereType(['Single', 'Group'])->where('status', [$i + 1])->count());
    }
    $collection->put("cancel", $this->repo->whereType(['Single', 'Group'])->where('status', [6, 7])->count());

    return json_decode($collection);
  }

  public function searchTasksTitle($query)
  {
    try {
      //code...
      return $this->repo->query()->where(function ($queryBuilder) use ($query) {
        $queryBuilder->where('judul_project', 'like', '%' . $query . '%');
        // ->orWhere('column2', 'like', '%' . $query . '%')
        // ->orWhere('column3', 'like', '%' . $query . '%');
        // Add more columns if needed
      })->with(['companies', 'clients.companyclients', 'divisions', 'users'])
        ->orderBy('id', 'desc')
        ->orderBy('status', 'desc')
        ->get();
    } catch (\Throwable $th) {
      //throw $th;
      return;
    }
  }
}
