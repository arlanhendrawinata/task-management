<?php 
namespace App\Services;

use App\Repositories\UserRepository;

class UserService{
  public function __construct(protected UserRepository $repo)
  {}

  public function getAll(){
    return $this->repo->all();
  }

  public function getSelectedColumns(){
    return $this->repo->selectColumns(['id', 'nama']);
  }
}
?>