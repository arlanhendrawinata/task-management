<?php 
namespace App\Services;

use App\Repositories\ClientRepository;

class ClientService {
  public function __construct(protected ClientRepository $repo){}

  public function getAll(){
    return $this->repo->all();
  }

  public function getWhereStatus($status){
    return $this->repo->where('status', $status)->get();
  }
  
  public function firstOrFailWhere($status){
    return $this->repo->where('status', $status)->firstOrFail();
  }

  public function getSelectedColumns(){
    return $this->repo->selectColumns(['id', 'nama_client']);
  }
}
?>