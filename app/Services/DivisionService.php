<?php 
namespace App\Services;

use App\Repositories\DivisionRepository;

class DivisionService{
  public function __construct(protected DivisionRepository $repo)
  {}

  public function getAll(){
    return $this->repo->all();
  }

  public function getSelectedColumns(){
    return $this->repo->selectColumns(['id', 'nama_divisi']);
  }

  public function getWhereStatus($status){
    return $this->repo->where('status', $status)->get();
  }
}
?>