<?php 

namespace App\Services;

use App\Repositories\PicRepository;

class PicService{
  public function __construct(protected PicRepository $repo)
  {}

  public function getWith(array $with){
    return $this->repo->with($with)->get();
  }
}

?>