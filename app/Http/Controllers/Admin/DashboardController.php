<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Client;
use App\Models\Company;
use App\Models\Division;
use App\Models\Pic;
use App\Models\Project;
use App\Models\Userdetail;
use App\Models\User;
use App\Repositories\PicRepository;
use App\Services\ClientService;
use App\Services\DivisionService;
use App\Services\PicService;
use App\Services\ProjectService;
use App\Services\UserService;
use Carbon\Carbon;
use DB;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    function __construct(
        protected ProjectService $projectService,
        protected PicService $picService,
        protected ClientService $clientService,
        protected UserService $userService,
        protected DivisionService $divisionService,
    ) {
    }

    public function index()
    {
        $title = 'Dashboard';
        $tableTitle = "New Task";

        $projects = $this->projectService->getWith_IdStatus(['companies', 'clients', 'divisions', 'users']);
        $countStatus = $this->projectService->collectCountStatus();
        $pics = $this->picService->getWith(['projects', 'users']);

        $clients = $this->clientService->getAll();
        $users = $this->userService->getAll();
        $divisions = $this->divisionService->getAll();
        
        $currentUrl = " ";
        $name = "";
        $val = "";

        return view('admin.dashboard', compact('name', 'val', 'currentUrl', 'divisions', 'users', 'clients', 'projects', 'pics', 'title', 'countStatus', 'tableTitle'));
    }

    public function datatable(){
        $projects = $this->projectService->getWith_IdStatus(['companies', 'clients', 'divisions', 'users']);
        return DataTables::of($projects)
        ->addIndexColumn()
        ->addColumn('tgl_input', function($project){
            if ($project->tgl_input != null) {
                $date_input = date('d/m/y', strtotime($project->tgl_input));
                return $date_input;
            }
        })
        ->addColumn('pic', function($project) {
            $pics = $this->picService->getWith(['users', 'projects']);
            $names = array();
            foreach ($pics as $pic) {
                if($pic->project_id == $project->id) array_push($names, initial($pic->users->nama));
            }
            return implode(', ', $names);
        })
        ->addColumn('task', function($project){
            return '<a href="'. route('admin-task-single', ['id' => $project->id]) .'" class="btn-task">'. ucwords(strtolower($project->judul_project)) .'</a>';
        })
        ->addColumn('task_target', function($project){
            return date('d/m/y', strtotime($project->estimasi));
            // $dateinput = strtotime($project->tgl_input);
            // $dateestimasi = strtotime($project->estimasi);
            // $secs = $dateestimasi - $dateinput;
            // $dayestimasi = $secs / 86400;
            // return $dayestimasi; 
        })
        ->addColumn('tgl_mulai', function($project){
            if ($project->tgl_mulai != null) {
                $date_mulai = date('d/m/y', strtotime($project->tgl_mulai));
                return $date_mulai;
            }
        })
        ->addColumn('tgl_selesai', function($project){
            if ($project->tgl_selesai != null) {
                $date_selesai = date('d/m/y', strtotime($project->tgl_selesai));
                return $date_selesai;
            }
        })
        ->addColumn('task_result', function($project){
            if ($project->tgl_selesai != null) {
                $dateselesai = strtotime($project->tgl_selesai);
                $dateestimasi2 = strtotime($project->estimasi);
                $secs = $dateestimasi2 - $dateselesai;
                $dayresult = $secs / 86400;
                return $dayresult;
            } else {
                return '-';
            }
        })
        ->addColumn('task_detail', function($project){
            return Str::limit($project->detail_project, 50);
        })
        ->addColumn('task_status', function($project){
            $statusClass = "infostatus bg-".status_color()[$project->status];
            return '<a href="javascript:void(0)"><div class="'. $statusClass .'">'. read_status()[$project->status] .'</div>';
        })
        ->rawColumns(['task', 'task_status'])
        ->make(true);
    }

    public function search(Request $request)
    {
        if ($request->input('status') == 1) {
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->whereIn("status", [1])
                ->whereIn('type', ['Single', 'Group'])
                ->orderBy('id', 'desc');
            $projects = $projects->paginate(10)->appends([
                'status' => $request->status
            ]);

            $tableTitle = "List Task Active";
        }

        if ($request->input('status') == 2) {
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->whereIn("status", [2])
                ->whereIn('type', ['Single', 'Group'])
                ->orderBy('id', 'desc');
            $projects = $projects->paginate(10)->appends([
                'status' => $request->status
            ]);

            $tableTitle = "List Task Progress";
        }

        if ($request->input('status') == 3) {
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->whereIn("status", [3])
                ->whereIn('type', ['Single', 'Group'])
                ->orderBy('id', 'desc');
            $projects = $projects->paginate(10)->appends([
                'status' => $request->status
            ]);

            $tableTitle = "List Task Submited";
        }

        if ($request->input('status') == 4) {
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->whereIn("status", [4])
                ->whereIn('type', ['Single', 'Group'])
                ->orderBy('id', 'desc');
            $projects = $projects->paginate(10)->appends([
                'status' => $request->status
            ]);

            $tableTitle = "List Task Verified";
        }

        if ($request->input('status') == 5) {
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->whereIn("status", [5])
                ->whereIn('type', ['Single', 'Group'])
                ->orderBy('id', 'desc');
            $projects = $projects->paginate(10)->appends([
                'status' => $request->status
            ]);

            $tableTitle = "List Task Success";
        }

        if ($request->input('status') == 6) {
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->whereIn("status", [6, 7])
                ->whereIn('type', ['Single', 'Group'])
                ->orderBy('id', 'desc');
            $projects = $projects->paginate(10)->appends([
                'status' => $request->status
            ]);

            $tableTitle = "List Task Cancelled";
        }

        $pics = Pic::with('projects', 'users')->get();
        $title = 'Task';

        $countStatus = $this->projectService->collectCountStatus();

        $currentUrl = "search";
        $clients = Client::all();
        $users = User::all();
        $divisions = Division::all();
        $name = "";
        $val = "";

        return view('admin.dashboard', compact('name', 'val', 'divisions', 'currentUrl', 'projects', 'clients', 'users', 'pics', 'title',  'countStatus', 'tableTitle'));
    }

    public function filter(Request $request)
    {
        // return $request->input('division');

        $fromDate = $request->from_date;
        $toDate = $request->to_date;


        if ($request->input('from_date') != NULL && $request->input('to_date') == NULL && $request->input('client') == NULL && $request->input('division') == NULL) {
            // return 'from';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", $fromDate)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'from_date' => $fromDate,
            ]);
        } elseif ($request->input('to_date') != NULL && $request->input('from_date') == NULL && $request->input('client') == NULL && $request->input('division') == NULL) {
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", $toDate)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'to_date' => $toDate,
            ]);
        } elseif ($request->input('client') != NULL && $request->input('to_date') == NULL && $request->input('from_date') == NULL && $request->input('division') == NULL) {
            // return 'client';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("client_id", $request->client)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'client' => $request->client,
            ]);
        } elseif ($request->input('division') != NULL && $request->input('to_date') == NULL && $request->input('from_date') == NULL && $request->input('client') == NULL) {
            // return 'division';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("divisi_id", $request->division)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'division' => $request->division,
            ]);
        } elseif ($request->input('from_date') != NULL && $request->input('client') != NULL && $request->input('to_date') == NULL && $request->input('division') == NULL) {
            // return 'from, client';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", $fromDate)
                ->where("client_id", $request->client)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'from_date' => $fromDate,
                'client' => $request->client,
            ]);
        } elseif ($request->input('from_date') != NULL && $request->input('division') != NULL && $request->input('to_date') == NULL && $request->input('client') == NULL) {
            // return 'from, divisi';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", $fromDate)
                ->where("divisi_id", $request->division)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'from_date' => $fromDate,
                'division' => $request->division,
            ]);
        } elseif ($request->input('to_date') != NULL && $request->input('client') != NULL && $request->input('from_date') == NULL && $request->input('division') == NULL) {
            // return 'to, client';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", $toDate)
                ->where("client_id", $request->client)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'to_date' => $toDate,
                'client' => $request->client,
            ]);
        } elseif ($request->input('to_date') != NULL && $request->input('division') != NULL && $request->input('from_date') == NULL && $request->input('client') == NULL) {
            // return 'to, divisi';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", $toDate)
                ->where("divisi_id", $request->division)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'to_date' => $toDate,
                'division' => $request->division,
            ]);
        } elseif ($request->input('from_date') != NULL && $request->input('to_date') != NULL && $request->input('client') == NULL && $request->input('division') == NULL) {
            // return 'from, to';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", ">=", $fromDate)
                ->where("tgl_input", "<=", $toDate)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'from_date' => $fromDate,
                'to_date' => $toDate,
            ]);
        } elseif ($request->input('from_date') != NULL && $request->input('to_date') != NULL && $request->input('division') != NULL && $request->input('client') == NULL) {
            // return 'divisi, from, to';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", ">=", $fromDate)
                ->where("tgl_input", "<=", $toDate)
                ->where("divisi_id", $request->division)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'from_date' => $fromDate,
                'to_date' => $toDate,
                'divisi' => $request->division,
            ]);
        } elseif ($request->input('from_date') != NULL && $request->input('to_date') != NULL && $request->input('client') != NULL && $request->input('division') == NULL) {
            // return 'client, from, to';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", ">=", $fromDate)
                ->where("tgl_input", "<=", $toDate)
                ->where("client_id", $request->client)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'from_date' => $fromDate,
                'to_date' => $toDate,
                'client' => $request->client,
            ]);
        } elseif ($request->input('from_date') != NULL && $request->input('client') != NULL && $request->input('division') != NULL && $request->input('to_date') == NULL) {
            // return 'from, client, divisi';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", ">=", $fromDate)
                ->where("client_id", $request->client)
                ->where("divisi_id", $request->division)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'from_date' => $fromDate,
                'client' => $request->client,
                'division' => $request->division,
            ]);
        } elseif ($request->input('to_date') != NULL && $request->input('client') != NULL && $request->input('division') != NULL && $request->input('from_date') == NULL) {
            // return 'to, client, divisi';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", ">=", $toDate)
                ->where("client_id", $request->client)
                ->where("divisi_id", $request->division)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'to_date' => $toDate,
                'client' => $request->client,
                'division' => $request->division,
            ]);
        } elseif ($request->input('from_date') != NULL && $request->input('to_date') != NULL && $request->input('client') != NULL && $request->input('division') != NULL) {
            // return 'client, divisi, from, to';
            $projects = Project::with('companies', 'clients', 'divisions', 'users')
                ->where("tgl_input", ">=", $fromDate)
                ->where("tgl_input", "<=", $toDate)
                ->where("client_id", $request->client)
                ->where("divisi_id", $request->division)
                ->orderBy('status', 'asc');
            $projects = $projects->paginate(10)->appends([
                'from_date' => $fromDate,
                'to_date' => $toDate,
                'client' => $request->client,
                'division' => $request->division,
            ]);
        } else {
            return redirect()->back()->with('errors', 'Please fill out the form!');
        }

        $clients = Client::all();
        $divisions = Division::all();
        $pics = Pic::with('projects', 'users')->get();
        $title = 'Task';
        $name = "";
        $val = "";
        $users = User::all();
        $currentUrl = "search";
        $tableTitle = "Filter Task";

        $countStatus = $this->projectService->collectCountStatus();

        return view('admin.dashboard', compact('val', 'divisions', 'currentUrl', 'tableTitle', 'projects', 'users', 'clients', 'pics', 'title',  'countStatus', 'name'));
    }

    public function selectSearch($name, $val)
    {
        $title = 'Task';

        $projects = Project::with('companies', 'clients', 'divisions', 'users')
            ->where($name, $val)
            ->whereIn('type', ['Single', 'Group'])
            ->orderBy('id', 'desc');
        $projects = $projects->paginate(10);
        $pics = Pic::with('projects', 'users')->get();

        $projectName = Project::all()->pluck('judul_project')->toArray();

        $countStatus = $this->projectService->collectCountStatus();

        $tableTitle = "Search Task";

        $clients = Client::all();
        $users = User::all();
        $currentUrl = "search";
        $name = "";
        $divisions = Division::all();

        return view('admin.dashboard', compact('name', 'val', 'divisions', 'users', 'currentUrl', 'clients', 'projects', 'pics', 'title', 'countStatus', 'tableTitle'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public function dashaddpic(Request $request)
    // {
    //     Pic::create([
    //         'project_id' => $request->project_id,
    //         'user_id' => $request->user_id,
    //         'status' => 1,
    //     ]);

    //     return redirect()->back()->with('success', 'Pic successfully added');
    // }

    // public function dashdestroypic($id)
    // {
    //     $pic = Pic::find($id);
    //     $pic->delete();
    //     return redirect()->back()->with('success', 'Pic has been removed');
    // }

    // public function dashshowpic($id)
    // {
    //     $projectDivisi = Project::select('divisi_id')->where('id', $id)->first();
    //     $users = User::with('userdetail')->whereRelation('userdetail', 'divisi_id', $projectDivisi->divisi_id)->get();
    //     $pics = Pic::all();
    //     $project = Project::with('companies', 'clients', 'divisions', 'users')->where('id', $id)->first();
    //     return view('admin.pic', compact('project', 'pics', 'users'));
    // }

    // public function dashaddestimasi(Request $request)
    // {
    //     $estimasi = Project::find($request->project_id);

    //     $estimasi->update([
    //         'estimasi' => $request->estimasi,
    //     ]);
    //     return redirect()->route('dashboard')->with('success', 'Estimasi successfully added');
    // }

    public function dashstartdate(Request $request)
    {
        $startdate = Project::find($request->project_id);

        $startdate->update([
            'tgl_mulai' => $request->tgl_mulai,
            'status' => 2,
        ]);
        return redirect()->back()->with('success', 'Task Started');
    }
}
