<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Services\ClientService;
use App\Services\CompanyService;
use App\Services\DivisionService;
use App\Services\PicService;
use App\Services\ProjectService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;

class TaskController extends Controller
{
    function __construct(
        // protected ProjectService $projectService,
        // protected PicService $picService,
        // protected DivisionService $divisionService,
        // protected ClientService $clientService,
        // protected UserService $userService,
        // protected CompanyService $companyService,
    ) {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $divisions = $this->divisionService->getAll();
        return view('tasks.index');
    }

    public function datatable($isSearch = false, $search = "")
    {
        $projects = $this->projectService->getWith_IdStatus(['companies', 'clients', 'divisions', 'users']);

        if ($isSearch) {
            $projects = $this->projectService->searchTasksTitle($search);
        }

        return DataTables::of($projects)
            ->addIndexColumn()
            ->addColumn('tgl_input', function ($project) {
                if ($project->tgl_input != null) {
                    $date_input = date('d/m/y', strtotime($project->tgl_input));
                    return $date_input;
                }
            })
            ->addColumn('pic', function ($project) {
                $pics = $this->picService->getWith(['users', 'projects']);
                $names = array();
                foreach ($pics as $pic) {
                    if ($pic->project_id == $project->id) array_push($names, initial($pic->users->nama));
                }
                return implode(', ', $names);
            })
            ->addColumn('task', function ($project) {
                return '<a href="' . route('admin-task-single', ['id' => $project->id]) . '" class="text-dark">' . Str::limit($project->judul_project, 40, '...') . '<span class="badge badge-light-success ms-2">' . $project->divisions->nama_divisi . '</span><span class="badge badge-light-secondary ms-2">' . $project->type . '</span></a>';
            })
            ->addColumn('task_target', function ($project) {
                return date('d/m/y', strtotime($project->estimasi));
                // $dateinput = strtotime($project->tgl_input);
                // $dateestimasi = strtotime($project->estimasi);
                // $secs = $dateestimasi - $dateinput;
                // $dayestimasi = $secs / 86400;
                // return $dayestimasi; 
            })
            ->addColumn('tgl_mulai', function ($project) {
                if ($project->tgl_mulai != null) {
                    $date_mulai = date('d/m/y', strtotime($project->tgl_mulai));
                    return $date_mulai;
                }
            })
            ->addColumn('tgl_selesai', function ($project) {
                if ($project->tgl_selesai != null) {
                    $date_selesai = date('d/m/y', strtotime($project->tgl_selesai));
                    return $date_selesai;
                }
            })
            ->addColumn('task_result', function ($project) {
                if ($project->tgl_selesai != null) {
                    $dateselesai = strtotime($project->tgl_selesai);
                    $dateestimasi2 = strtotime($project->estimasi);
                    $secs = $dateestimasi2 - $dateselesai;
                    $dayresult = $secs / 86400;
                    return $dayresult;
                } else {
                    return '';
                }
            })
            ->addColumn('task_detail', function ($project) {
                return Str::limit($project->detail_project, 50);
            })
            ->addColumn('task_status', function ($project) {
                $statusClass = status_color()[$project->status];
                // return '<a href="javascript:void(0)"><div class="'. $statusClass .'">'. read_status()[$project->status] .'</div>';
                return '<span class="badge py-3 px-4 fs-7 ' . $statusClass . '">' . read_status()[$project->status] . '</span>';
            })
            ->rawColumns(['task', 'task_status'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $divisions = $this->divisionService->getSelectedColumns();
        // $clients = $this->clientService->getSelectedColumns();
        // $users = $this->userService->getSelectedColumns();
        // $companies = $this->companyService->getSelectedColumns();
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => [
                'required',
                'string',
                function ($attribute, $value, $fail) {
                    // Perform preg_replace on the value
                    $plainText = preg_replace('/<(.|\n)*?>/', '', $value);
    
                    // Check if the value has changed after preg_replace
                    if (empty($plainText)) {
                        $fail('The :attribute field is required');
                    }
                },
            ],
        ]);
    
        if ($validator->fails()) {
            // Handle validation failure
            return response()->json(['errors' => $validator->errors()], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //
    }
}
