@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<!--begin::Row-->
<div class="row g-5 g-xl-10">
  <!--begin::Col-->
  <div class="col-xl-3 mb-md-5 mb-xl-10">
    <!--begin::Card widget 8-->
    <div class="card overflow-hidden mb-5 mb-xl-10">
      <!--begin::Card body-->
      <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
        <!--begin::Statistics-->
        <div class="mb-4 px-9">
          <!--begin::Info-->
          <div class="d-flex align-items-center mb-2">
            <!--begin::Currency-->
            <span class="fs-4 fw-semibold text-gray-500 align-self-start me-1&gt;">$</span>
            <!--end::Currency-->
            <!--begin::Value-->
            <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1">69,700</span>
            <!--end::Value-->
            <!--begin::Label-->
            <span class="badge badge-light-success fs-base">
              <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>2.2%</span>
              <!--end::Label-->
            </div>
          <!--end::Info-->
          <!--begin::Description-->
          <span class="fs-6 fw-semibold text-gray-500">Total Online Sales</span>
          <!--end::Description-->
        </div>
        <!--end::Statistics-->
        <!--begin::Chart-->
        <div id="kt_card_widget_8_chart" class="min-h-auto" style="height: 125px"></div>
        <!--end::Chart-->
      </div>
      <!--end::Card body-->
    </div>
    <!--end::Card widget 8-->
  </div>
  <!--end::Col-->
  <!--begin::Col-->
  <div class="col-xl-3 mb-md-5 mb-xl-10">
    <!--begin::Card widget 8-->
    <div class="card overflow-hidden mb-5 mb-xl-10">
      <!--begin::Card body-->
      <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
        <!--begin::Statistics-->
        <div class="mb-4 px-9">
          <!--begin::Info-->
          <div class="d-flex align-items-center mb-2">
            <!--begin::Currency-->
            <span class="fs-4 fw-semibold text-gray-500 align-self-start me-1&gt;">$</span>
            <!--end::Currency-->
            <!--begin::Value-->
            <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1">69,700</span>
            <!--end::Value-->
            <!--begin::Label-->
            <span class="badge badge-light-success fs-base">
              <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>2.2%</span>
              <!--end::Label-->
            </div>
            <!--end::Info-->
            <!--begin::Description-->
            <span class="fs-6 fw-semibold text-gray-500">Total Online Sales</span>
            <!--end::Description-->
          </div>
        <!--end::Statistics-->
        <!--begin::Chart-->
        <div id="kt_card_widget_9_chart" class="min-h-auto" style="height: 125px"></div>
        <!--end::Chart-->
      </div>
      <!--end::Card body-->
    </div>
    <!--end::Card widget 8-->
  </div>
  <!--end::Col-->
  <!--begin::Col-->
  <div class="col-xl-3 mb-md-5 mb-xl-10">
    <!--begin::Card widget 8-->
    <div class="card overflow-hidden mb-5 mb-xl-10">
      <!--begin::Card body-->
      <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
        <!--begin::Statistics-->
        <div class="mb-4 px-9">
          <!--begin::Info-->
          <div class="d-flex align-items-center mb-2">
            <!--begin::Currency-->
            <span class="fs-4 fw-semibold text-gray-500 align-self-start me-1&gt;">$</span>
            <!--end::Currency-->
            <!--begin::Value-->
            <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1">69,700</span>
            <!--end::Value-->
            <!--begin::Label-->
            <span class="badge badge-light-success fs-base">
              <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>2.2%</span>
            <!--end::Label-->
          </div>
          <!--end::Info-->
          <!--begin::Description-->
          <span class="fs-6 fw-semibold text-gray-500">Total Online Sales</span>
          <!--end::Description-->
        </div>
        <!--end::Statistics-->
        <!--begin::Chart-->
        <div id="kt_card_widget_8_chart" class="min-h-auto" style="height: 125px"></div>
        <!--end::Chart-->
      </div>
      <!--end::Card body-->
    </div>
    <!--end::Card widget 8-->
  </div>
  <!--end::Col-->
  <!--begin::Col-->
  <div class="col-md-6 col-xl-3 mb-xxl-10">
    <!--begin::Card widget 9-->
    <div class="card overflow-hidden mb-5 mb-xl-10">
      <!--begin::Card body-->
      <div class="card-body d-flex justify-content-between flex-column px-0 pb-0">
        <!--begin::Statistics-->
        <div class="mb-4 px-9">
          <!--begin::Statistics-->
          <div class="d-flex align-items-center mb-2">
            <!--begin::Value-->
            <span class="fs-2hx fw-bold text-gray-800 me-2 lh-1">29,420</span>
            <!--end::Value-->
            <!--begin::Label-->
            <span class="badge badge-light-success fs-base">
              <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>2.6%</span>
            <!--end::Label-->
          </div>
          <!--end::Statistics-->
          <!--begin::Description-->
          <span class="fs-6 fw-semibold text-gray-500">Total Online Visitors</span>
          <!--end::Description-->
        </div>
        <!--end::Statistics-->
        <!--begin::Chart-->
        <div id="kt_card_widget_9_chart" class="min-h-auto" style="height: 125px"></div>
        <!--end::Chart-->
      </div>
      <!--end::Card body-->
    </div>
    <!--end::Card widget 9-->
  </div>
  <!--end::Col-->
</div>
<!--end::Row-->
<!--begin::Row-->
<div class="row g-5 g-xl-10 g-xl-10">
  <!--begin::Col-->
  <div class="col-xl-6 mb-xl-10">
    <!--begin::List widget 8-->
    <div class="card card-flush h-xl-100">
      <!--begin::Header-->
      <div class="card-header pt-7 mb-5">
        <!--begin::Title-->
        <h3 class="card-title align-items-start flex-column">
          <span class="card-label fw-bold text-gray-800">New Tasks</span>
          <span class="text-gray-500 mt-1 fw-semibold fs-6">20 countries share 97% visits</span>
        </h3>
        <!--end::Title-->
        <!--begin::Toolbar-->
        <div class="card-toolbar">
          <a href="apps/ecommerce/sales/listing.html" class="btn btn-sm btn-light">All Tasks</a>
        </div>
        <!--end::Toolbar-->
      </div>
      <!--end::Header-->
      <!--begin::Body-->
      <div class="card-body pt-0">
        @for ($i = 0; $i < 7; $i++) <!--begin::Items-->
          <div class="m-0">
            <!--begin::Item-->

            <div class="d-flex flex-stack">
              <!--begin::Flag-->
              <i class="ki-duotone ki-book fs-2x m-4">
                <i class="path1"></i>
                <i class="path2"></i>
                <i class="path3"></i>
                <i class="path4"></i>
              </i>
              <!-- <img src="assets/media/flags/united-states.svg" class="me-4 w-25px" style="border-radius: 4px" alt="" /> -->
              <!--end::Flag-->
              <!--begin::Section-->
              <div class="d-flex flex-stack flex-row-fluid d-grid gap-2">
                <!--begin::Content-->
                <div class="me-5">
                  <!--begin::Title-->
                  <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">United States</a>
                  <!--end::Title-->
                  <!--begin::Desc-->
                  <span class="text-gray-500 fw-semibold fs-7 d-block text-start ps-0">Direct link
                    clicks</span>
                  <!--end::Desc-->
                </div>
                <!--end::Content-->
                <!--begin::Info-->
                <div class="d-flex align-items-center">
                  <!--begin::Number-->
                  <span class="text-gray-800 fw-bold fs-6 me-3 d-block">9,763</span>
                  <!--end::Number-->
                  <!--begin::Label-->
                  <div class="m-0">
                    <!--begin::Label-->
                    <span class="badge badge-light-success fs-base">
                      <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>2.6%</span>
                    <!--end::Label-->
                  </div>
                  <!--end::Label-->
                </div>
                <!--end::Info-->
              </div>
              <!--end::Section-->
            </div>
            <!--end::Item-->
            <!--begin::Separator-->
            <div class="separator separator-dashed my-3"></div>
            <!--end::Separator-->
          </div>
          <!--end::Items-->
          @endfor
      </div>
      <!--end::Body-->
    </div>
    <!--end::LIst widget 8-->
  </div>
  <!--end::Col-->
  <!--begin::Col-->
  <div class="col-xl-6 mb-xl-10">
    <!--begin::List widget 9-->
    <div class="card card-flush h-xl-100">
      <!--begin::Header-->
      <div class="card-header py-7">
        <!--begin::Title-->
        <h3 class="card-title align-items-start flex-column">
          <span class="card-label fw-bold text-gray-800">Progress Tasks</span>
          <span class="text-gray-500 mt-1 fw-semibold fs-6">8k social visitors</span>
        </h3>
        <!--end::Title-->
        <!--begin::Toolbar-->
        <div class="card-toolbar">
          <a href="#" class="btn btn-sm btn-light">Progress Tasks</a>
        </div>
        <!--end::Toolbar-->
      </div>
      <!--end::Header-->
      <!--begin::Body-->
      <div class="card-body card-body d-flex justify-content-between flex-column pt-3">
        <!--begin::Item-->
        <div class="d-flex flex-stack">
          <!--begin::Flag-->
          <img src="assets/media/svg/brand-logos/dribbble-icon-1.svg" class="me-4 w-30px" style="border-radius: 4px"
            alt="" />
          <!--end::Flag-->
          <!--begin::Section-->
          <div class="d-flex align-items-center flex-stack flex-wrap flex-row-fluid d-grid gap-2">
            <!--begin::Content-->
            <div class="me-5">
              <!--begin::Title-->
              <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Dribbble</a>
              <!--end::Title-->
              <!--begin::Desc-->
              <span class="text-gray-500 fw-semibold fs-7 d-block text-start ps-0">Community</span>
              <!--end::Desc-->
            </div>
            <!--end::Content-->
            <!--begin::Wrapper-->
            <div class="d-flex align-items-center">
              <!--begin::Number-->
              <span class="text-gray-800 fw-bold fs-4 me-3">579</span>
              <!--end::Number-->
              <!--begin::Info-->
              <div class="m-0">
                <!--begin::Label-->
                <span class="badge badge-light-success fs-base">
                  <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>2.6%</span>
                <!--end::Label-->
              </div>
              <!--end::Info-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Section-->
        </div>
        <!--end::Item-->
        <!--begin::Separator-->
        <div class="separator separator-dashed my-3"></div>
        <!--end::Separator-->
        <!--begin::Item-->
        <div class="d-flex flex-stack">
          <!--begin::Flag-->
          <img src="assets/media/svg/brand-logos/linkedin-1.svg" class="me-4 w-30px" style="border-radius: 4px"
            alt="" />
          <!--end::Flag-->
          <!--begin::Section-->
          <div class="d-flex align-items-center flex-stack flex-wrap flex-row-fluid d-grid gap-2">
            <!--begin::Content-->
            <div class="me-5">
              <!--begin::Title-->
              <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Linked In</a>
              <!--end::Title-->
              <!--begin::Desc-->
              <span class="text-gray-500 fw-semibold fs-7 d-block text-start ps-0">Social Media</span>
              <!--end::Desc-->
            </div>
            <!--end::Content-->
            <!--begin::Wrapper-->
            <div class="d-flex align-items-center">
              <!--begin::Number-->
              <span class="text-gray-800 fw-bold fs-4 me-3">2,588</span>
              <!--end::Number-->
              <!--begin::Info-->
              <div class="m-0">
                <!--begin::Label-->
                <span class="badge badge-light-danger fs-base">
                  <i class="ki-outline ki-arrow-down fs-5 text-danger ms-n1"></i>0.4%</span>
                <!--end::Label-->
              </div>
              <!--end::Info-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Section-->
        </div>
        <!--end::Item-->
        <!--begin::Separator-->
        <div class="separator separator-dashed my-3"></div>
        <!--end::Separator-->
        <!--begin::Item-->
        <div class="d-flex flex-stack">
          <!--begin::Flag-->
          <img src="assets/media/svg/brand-logos/slack-icon.svg" class="me-4 w-30px" style="border-radius: 4px"
            alt="" />
          <!--end::Flag-->
          <!--begin::Section-->
          <div class="d-flex align-items-center flex-stack flex-wrap flex-row-fluid d-grid gap-2">
            <!--begin::Content-->
            <div class="me-5">
              <!--begin::Title-->
              <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Slack</a>
              <!--end::Title-->
              <!--begin::Desc-->
              <span class="text-gray-500 fw-semibold fs-7 d-block text-start ps-0">Messanger</span>
              <!--end::Desc-->
            </div>
            <!--end::Content-->
            <!--begin::Wrapper-->
            <div class="d-flex align-items-center">
              <!--begin::Number-->
              <span class="text-gray-800 fw-bold fs-4 me-3">794</span>
              <!--end::Number-->
              <!--begin::Info-->
              <div class="m-0">
                <!--begin::Label-->
                <span class="badge badge-light-success fs-base">
                  <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>0.2%</span>
                <!--end::Label-->
              </div>
              <!--end::Info-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Section-->
        </div>
        <!--end::Item-->
        <!--begin::Separator-->
        <div class="separator separator-dashed my-3"></div>
        <!--end::Separator-->
        <!--begin::Item-->
        <div class="d-flex flex-stack">
          <!--begin::Flag-->
          <img src="assets/media/svg/brand-logos/youtube-3.svg" class="me-4 w-30px" style="border-radius: 4px" alt="" />
          <!--end::Flag-->
          <!--begin::Section-->
          <div class="d-flex align-items-center flex-stack flex-wrap flex-row-fluid d-grid gap-2">
            <!--begin::Content-->
            <div class="me-5">
              <!--begin::Title-->
              <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">YouTube</a>
              <!--end::Title-->
              <!--begin::Desc-->
              <span class="text-gray-500 fw-semibold fs-7 d-block text-start ps-0">Video
                Channel</span>
              <!--end::Desc-->
            </div>
            <!--end::Content-->
            <!--begin::Wrapper-->
            <div class="d-flex align-items-center">
              <!--begin::Number-->
              <span class="text-gray-800 fw-bold fs-4 me-3">1,578</span>
              <!--end::Number-->
              <!--begin::Info-->
              <div class="m-0">
                <!--begin::Label-->
                <span class="badge badge-light-success fs-base">
                  <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>4.1%</span>
                <!--end::Label-->
              </div>
              <!--end::Info-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Section-->
        </div>
        <!--end::Item-->
        <!--begin::Separator-->
        <div class="separator separator-dashed my-3"></div>
        <!--end::Separator-->
        <!--begin::Item-->
        <div class="d-flex flex-stack">
          <!--begin::Flag-->
          <img src="assets/media/svg/brand-logos/instagram-2-1.svg" class="me-4 w-30px" style="border-radius: 4px"
            alt="" />
          <!--end::Flag-->
          <!--begin::Section-->
          <div class="d-flex align-items-center flex-stack flex-wrap flex-row-fluid d-grid gap-2">
            <!--begin::Content-->
            <div class="me-5">
              <!--begin::Title-->
              <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Instagram</a>
              <!--end::Title-->
              <!--begin::Desc-->
              <span class="text-gray-500 fw-semibold fs-7 d-block text-start ps-0">Social
                Network</span>
              <!--end::Desc-->
            </div>
            <!--end::Content-->
            <!--begin::Wrapper-->
            <div class="d-flex align-items-center">
              <!--begin::Number-->
              <span class="text-gray-800 fw-bold fs-4 me-3">3,458</span>
              <!--end::Number-->
              <!--begin::Info-->
              <div class="m-0">
                <!--begin::Label-->
                <span class="badge badge-light-success fs-base">
                  <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>8.3%</span>
                <!--end::Label-->
              </div>
              <!--end::Info-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Section-->
        </div>
        <!--end::Item-->
        <!--begin::Separator-->
        <div class="separator separator-dashed my-3"></div>
        <!--end::Separator-->
        <!--begin::Item-->
        <div class="d-flex flex-stack">
          <!--begin::Flag-->
          <img src="assets/media/svg/brand-logos/facebook-3.svg" class="me-4 w-30px" style="border-radius: 4px"
            alt="" />
          <!--end::Flag-->
          <!--begin::Section-->
          <div class="d-flex align-items-center flex-stack flex-wrap flex-row-fluid d-grid gap-2">
            <!--begin::Content-->
            <div class="me-5">
              <!--begin::Title-->
              <a href="#" class="text-gray-800 fw-bold text-hover-primary fs-6">Facebook</a>
              <!--end::Title-->
              <!--begin::Desc-->
              <span class="text-gray-500 fw-semibold fs-7 d-block text-start ps-0">Social
                Network</span>
              <!--end::Desc-->
            </div>
            <!--end::Content-->
            <!--begin::Wrapper-->
            <div class="d-flex align-items-center">
              <!--begin::Number-->
              <span class="text-gray-800 fw-bold fs-4 me-3">2,047</span>
              <!--end::Number-->
              <!--begin::Info-->
              <div class="m-0">
                <!--begin::Label-->
                <span class="badge badge-light-success fs-base">
                  <i class="ki-outline ki-arrow-up fs-5 text-success ms-n1"></i>1.9%</span>
                <!--end::Label-->
              </div>
              <!--end::Info-->
            </div>
            <!--end::Wrapper-->
          </div>
          <!--end::Section-->
        </div>
        <!--end::Item-->
      </div>
      <!--end::Body-->
    </div>
    <!--end::List widget 9-->
  </div>
  <!--end::Col-->
</div>
<!--end::Row-->
@endsection
