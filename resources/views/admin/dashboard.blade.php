@extends('layouts.app')

@section('container.isi')
@section('active_dash', 'mm-active')


<style>
    .tasktable th {
        font-size: 11px !important;
        padding: 10px 5px !important;
        text-align: center;
    }

    .custom-w-10{
        width: 10% !important;
    }
    
    .tasktable .th_type{
        width: 2% !important;
    }

    .tasktable td {
        padding: 10px 4px !important;
        text-align: center;
        font-size: 11px;
    }

    .box {
        background-color: green;
        width: 10px;
        height: 25px;
    }

    .content-body .container-fluid {
        padding-left: 20px;
        padding-right: 20px;
    }

    .infostatus {
        padding: 3px 7px;
        border-radius: 4px;
        font-size: 0.613rem;
        color: white;
        width: 90px;
        margin: 0 auto;
    }

    .info-success {
        background-color: #2bc155;
    }

    .info-info {
        background-color: #2f4cdd;
    }

    .info-danger {
        background-color: #f35757;
    }

    .wide-dialog {
        max-width: 100%;
        margin: 20px 20px;
    }

    .btn-select {
        padding: 0.525rem 1.2rem;
        font-size: 0.713rem;
    }

    .dropdown-menu {
        min-width: 0;
        padding: 0;
    }

    .dropdown-menu .dropdown-item {
        font-size: 0.713rem;
    }

    .bg-grey {
        background-color: #D2D6D9;
        color: #44494B;
    }

    .bg-teal {
        background-color: #20c997;

    }

    .bg-yellow {
        background-color: #FCE83A !important;
        color: #917217;
    }

    .bg-orange {
        background-color: #ff9900 !important;
    }

    .tasktable .td-left {
        text-align: left;
        padding-left: 10px !important;
    }

    .btn-detail2:hover {
        color: #7e7e7e;
    }

    .btn-status {
        background: transparent;
        border: none;
    }

    .btn-status i {
        color: white;
        font-size: 30px;
    }

    .btn-status:hover {
        transform: scale(1.2);
        transition: transform .2s;
    }

    .widget-stat .media>span {
        width: 60px;
        height: 60px;
        min-width: 60px;
    }

    .widget-stat .media .media-body p {
        font-size: 11px;
        font-weight: 400;
    }

    .btn-task {
        padding: 3px 3px;
        border-radius: 4px;
        font-size: 0.713rem;
        color: white;
        width: 90px;
        margin: 0 auto;
    }

    .btn-task {
        text-decoration: blue underline;
        color: rgb(66, 103, 255);
    }

    .select2-container--default .select2-selection--single {
        width: 160px;
        border: 1px solid #d7dae3;
        border-radius: 0.75rem;
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button.previous{
        background-color: #0e185f !important;
        color: white !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.next{
        background-color: #0e185f !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button.disabled{
        background-color: #969ba0 !important;
        color: white !important;
    }
</style>

<div class="container-fluid">
    <div class="row page-titles mx-0">
        <div class="col-sm-6 p-md-0">
            <div class="welcome-text">
                <h4>Hi, {{ Auth::user()->nama }}!</h4>
                <span>Welcome to {{ $title }}</span>
            </div>
        </div>
    </div>

    <div class="row row-cols-6">
        <div class="col">
            <div class="widget-stat card bg-info">
                <div class="card-body p-4">
                    <div class="media">
                        <span class="mr-3">
                            <form action="{{ route('task-status-search')}}" method="GET">
                                <input type="number" value="1" hidden name="status">
                                <button class="btn-status" type="submit">
                                    <i class="flaticon-381-folder-6"></i>
                                </button>
                            </form>
                        </span>
                        <div class="media-body text-white text-right">
                            <p class="mb-1">Active</p>
                            <h3 class="text-white">{{ $countStatus->active }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="widget-stat card bg-warning">
                <div class="card-body p-4">
                    <div class="media">
                        <span class="mr-3">
                            <form action="{{ route('task-status-search') }}" method="GET">
                                <input type="number" value="2" hidden name="status">
                                <button class="btn-status" type="submit">
                                    <i class="flaticon-381-repeat-1"></i>
                                </button>
                            </form>
                        </span>
                        <div class="media-body text-white text-right">
                            <p class="mb-1">Progress</p>
                            <h3 class="text-white">{{ $countStatus->progress }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="widget-stat card bg-orange">
                <div class="card-body p-4">
                    <div class="media">
                        <span class="mr-3">
                            <form action="{{ route('task-status-search') }}" method="GET">
                                <input type="number" value="3" hidden name="status">
                                <button class="btn-status" type="submit">
                                    <i class="flaticon-381-folder-3"></i>
                                </button>
                            </form>
                        </span>
                        <div class="media-body text-white text-right">
                            <p class="mb-1">Submited</p>
                            <h3 class="text-white">{{ $countStatus->submit }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="widget-stat card bg-teal">
                <div class="card-body p-4">
                    <div class="media">
                        <span class="mr-3">
                            <form action="{{ route('task-status-search') }}" method="GET">
                                <input type="number" value="4" hidden name="status">
                                <button class="btn-status" type="submit">
                                    <i class="flaticon-381-stopwatch"></i>
                                </button>
                            </form>
                        </span>
                        <div class="media-body text-white text-right">
                            <p class="mb-1">Approved</p>
                            <h3 class="text-white">{{ $countStatus->verif }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="widget-stat card bg-success">
                <div class="card-body p-4">
                    <div class="media">
                        <span class="mr-3">
                            <form action="{{ route('task-status-search') }}" method="GET">
                                <input type="number" value="5" hidden name="status">
                                <button class="btn-status" type="submit">
                                    <i class="flaticon-381-success"></i>
                                </button>
                            </form>
                        </span>
                        <div class="media-body text-white text-right">
                            <p class="mb-1">Success</p>
                            <h3 class="text-white">{{ $countStatus->success }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="widget-stat card bg-danger">
                <div class="card-body p-4">
                    <div class="media">
                        <span class="mr-3">
                            <form action="{{ route('task-status-search') }}" method="GET">
                                <input type="number" value="6" hidden name="status">
                                <button class="btn-status" type="submit">
                                    <i class="flaticon-381-warning-1"></i>
                                </button>
                            </form>
                        </span>
                        <div class="media-body text-white text-right">
                            <p class="mb-1">Cancelled</p>
                            <h3 class="text-white">{{ $countStatus->cancel }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="tasktable table table-striped mb-3" id="dtProject">
                            <thead>
                                <tr class="tre">
                                    <!-- <th style="width: 10%;">No</th> -->
                                    <!-- <th>Company</th> -->
                                    <!-- <th>Client</th> -->
                                    <th>Publisher</th>
                                    <!-- <th>Input</th> -->
                                    <th>Team</th>
                                    <th>PIC</th>
                                    <th>Task</th>
                                    <th class="th_target">T</th>
                                    <!-- <th>Start</th> -->
                                    <!-- <th>End</th> -->
                                    <!-- <th>R</th> -->
                                    <!-- <th>Detail</th> -->
                                    <th class="th_type">Type</th>
                                    <th class="th_status">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal Start Date -->
@foreach($projects as $item)
<div class="modal fade" id="tambahstartModal{{$item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header d-flex align-items-center">
                <h5 class="modal-title" id="exampleModalLabel">Add Start Date</h5>
                <button type="button" class="btn btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
            </div>
            <form action="{{ route('dash-start-date') }}" method="post">
                @method('put')
                @csrf
                <input type="text" name="project_id" value="{{$item -> id}}" hidden>
                <div class="modal-body">
                    <div class="row">
                        <!--<div class="form-group row">-->
                        <label class="col-lg-4 col-form-label" for="tgl_mulai">Start Date
                            <span class="text-danger">*</span>
                        </label>
                        <div class="col-lg-6">
                            <input type="date" class="form-control" id="tgl_mulai" name="tgl_mulai" placeholder="Project start date">
                        </div>
                        <!--</div>-->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btnaddstartdate">Start</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endforeach

<!-- Tambah pic modal -->
<div class="modal fade" id="tambahPICModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header d-flex align-items-center">
                <h5 class="modal-title" id="exampleModalLabel">Add PIC</h5>
                <button type="button" class="btn btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
            </div>
            <div class="picModalForm">
            </div>
        </div>
    </div>
</div>

<!-- delete modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header d-flex align-items-center">
                <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                <button type="button" class="btn btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
            </div>
            <div class="modal-body">
                Are you sure to delete this data?
            </div>
            <div class="modal-footer modal-footer-delete">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end delete modal -->


<!-- Filter Modal -->
<form action="{{ route('admin-search') }}" method="get">
    <div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header d-flex align-items-center">
                    <h5 class="modal-title" id="exampleModalLabel">Filter Task</h5>
                    <button type="button" class="btn btn-close" data-bs-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
                </div>
                <div class="modal-body">
                    <div class="d-flex flex-column">
                        <div class="filter-date d-flex flex-column align-items-start ">
                            <h5 class="border-bottom mb-3 pb-3 w-100">By date</h5>
                            <div class="d-flex align-items-center">
                                <label class="" for="val-from_date">From date :</label>
                                <div class="col">
                                    <input type="date" class="form-control" id="val-from_date" value="" name="from_date">
                                </div>
                                <label class="" for="val-to_date">To date :</label>
                                <div class="col">
                                    <input type="date" class="form-control" id="val-to_date" value="" name="to_date">
                                </div>
                            </div>
                        </div>
                        <div class="filter-client mt-5">
                            <h5 class="border-bottom mb-3 pb-3">By client</h5>
                            <div class="d-flex align-items-center">
                                <label class="col-form-label" for="val-client">Client :
                                </label>
                                <div class="col-lg-6">
                                    <select class="tom-select-client" id="val-clients" name="client">
                                        <option value="">Please select client</option>
                                        @forelse($clients as $client)
                                        <option value="{{ $client->id }}">{{ $client->nama_client }}</option>
                                        @empty
                                        <option value="">No Datas</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="filter-division mt-5">
                            <h5 class="border-bottom mb-3 pb-3">By team</h5>
                            <div class="d-flex align-items-center">
                                <label class="col-form-label" for="val-division">Team :
                                </label>
                                <div class="col-lg-6">
                                    <select class="tom-select-division" id="val-divisions" name="division">
                                        <option value="">Please select team</option>
                                        @forelse($divisions as $division)
                                        <option value="{{ $division->id }}">{{ $division->nama_divisi }}
                                        </option>
                                        @empty
                                        <option value="">No Datas</option>
                                        @endforelse
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search mr-2"></i>Filter</button>
                </div>
            </div>
        </div>
    </div>
</form>



@section('scriptjs')
<script>
    $(document).ready(function() {
        // Setup - add a text input to each footer cell
        $('#dtProject thead tr')
            .clone(true)
            .addClass('filters')
            .appendTo('#dtProject thead');

        var table = $('#dtProject').DataTable({
            autoWidth: false,
            responsive: true,
            orderCellsTop: true,
            fixedHeader: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('dashboard.datatable') }}",
            columns: [
                // { "data": 'DT_RowIndex', "name": 'DT_RowIndex', "orderable": false, "searchable": false },
                // { "data": "clients.companyclients.name", "name": "clients.companyclients.name"},
                // { "data": "clients.nama_client", "name": "clients.nama_client"  },
                { "data": "users.nama", "name": "users.nama" },
                // { "data": "tgl_input", "name": "tgl_input" },
                { "data": "divisions.nama_divisi", "name": "divisions.nama_divisi" },
                { "data": "pic", "name": "pic" },
                { "data": "task", "name": "task" },
                { "data": "task_target", "name": "task_target" },
                // { "data": "tgl_mulai", "name": "tgl_mulai" },
                // { "data": "tgl_selesai", "name": "tgl_selesai" },
                // { "data": "task_result", "name": "task_result" },
                // { "data": "task_detail", "name": "task_detail" },
                { "data": "type", "name": "type" },
                { "data": "task_status", "name": "task_status"},
                // Add more columns as needed
            ],
            initComplete: function() {
                var api = this.api();

                // For each column
                api
                    .columns()
                    .eq(0)
                    .each(function(colIdx) {
                        // Set the header cell to contain the input element
                        var cell = $('.filters th').eq(
                            $(api.column(colIdx).header()).index()
                        );
                        var title = $(cell).text();
                        $(cell).html('<input type="text" placeholder="' + title + '" />');

                        // On every keypress in this input
                        $(
                            'input',
                            $('.filters th').eq($(api.column(colIdx).header()).index())
                            )
                            .off('keyup change')
                            .on('change', function(e) {
                                // Get the search value
                                $(this).attr('title', $(this).val());
                                var regexr = '({search})'; //$(this).parents('th').find('select').val();

                                var cursorPosition = this.selectionStart;
                                // Search the column for that value
                                api
                                    .column(colIdx)
                                    .search(
                                        this.value != '' ?
                                        regexr.replace('{search}', '(((' + this.value + ')))') :
                                        '',
                                        this.value != '',
                                        this.value == ''
                                    )
                                    .draw();
                            })
                            .on('keyup', function(e) {
                                e.stopPropagation();

                                $(this).trigger('change');
                                $(this)
                                    .focus()[0]
                                    .setSelectionRange(cursorPosition, cursorPosition);
                            });
                    });
            },
        });
    });

    $('.js-example-basic-single').select2();

    $(".select-type").change(function() {
        let selectVal = $(this).val();
        let url = "/dashboard/search/type/" + selectVal;
        window.location.href = url;
    });
    $(".select-priority").change(function() {
        let selectVal = $(this).val();
        let url = "/dashboard/search/prioritas/" + selectVal;
        window.location.href = url;
    });
    $(".select-status").change(function() {
        let selectVal = $(this).val();
        let url = "/dashboard/search/status/" + selectVal;
        window.location.href = url;
    });
    $(".select-divisi").change(function() {
        let selectVal = $(this).val();
        let url = "/dashboard/search/divisi_id/" + selectVal;
        window.location.href = url;
    });
</script>
<script>
    $(document).on('click', '.btn-addpic', function(event) {
        event.preventDefault();
        let id = $(this).attr('data-id');
        let url = $(this).attr('data-url');
        $('#detailModal').modal('hide');
        $('#tambahPICModal').modal('show');
        $.ajax({
            type: "GET",
            url: url,
            success: function(response) {
                $('.picModalForm').html('');
                $('.picModalForm').html(response);
            }
        });
    });
</script>
@endsection

@endsection