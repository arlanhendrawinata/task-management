@extends('layouts.app')
@section('title', 'All Tasks')
@section('content')
<!--begin::Row-->
<div class="row g-5 g-xl-8">
    <!--begin::Tables Widget 9-->
    <div class="card mb-5 mb-xl-8">
        <!--begin::Header-->
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bold fs-3 mb-1">All Tasks</span>
                {{-- <span class="text-muted mt-1 fw-semibold fs-7">Over 500 members</span> --}}
            </h3>
        </div>
        <!--end::Header-->
        <!--begin::Header2-->
        <div class="card-header border-0 pt-5 d-flex flex-stack mb-5">
            <!--begin::Search-->
            <div class="d-flex align-items-center position-relative my-1">
                <i class="ki-solid ki-magnifier fs-2 position-absolute ms-4"></i>
                <input type="text" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14"
                    placeholder="Search Tasks" id="searchInput" />
            </div>
            <!--end::Search-->

            <!--begin::Toolbar-->
            <div class="d-flex justify-content-end" data-kt-docs-table-toolbar="base">
                <!--begin::Filter-->
                <button type="button" class="btn btn-sm btn-light-primary me-3" data-kt-menu-trigger="click"
                    data-kt-menu-placement="bottom-end">
                    <i class="ki-duotone ki-filter fs-2"><span class="path1"></span><span class="path2"></span></i>
                    Filter
                </button>
                <!--end::Filter-->
                <!--begin::Menu dropdown-->
                <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true"
                    id="kt_menu_select2">
                    <!--begin::Header-->
                    <div class="px-7 py-5">
                        <div class="fs-5 text-gray-900 fw-bold">Filter Options</div>
                    </div>
                    <!--end::Header-->

                    <!--begin::Menu separator-->
                    <div class="separator border-gray-200"></div>
                    <!--end::Menu separator-->

                    <!--begin::Form-->
                    <div class="px-7 py-5">
                        <!--begin::Input group-->
                        <div class="mb-10">
                            <!--begin::Label-->
                            <label class="form-label fw-semibold">Status:</label>
                            <!--end::Label-->

                            <!--begin::Input-->
                            <div>
                                <select class="form-select form-select-solid" multiple data-kt-select2="true" multiple
                                    data-close-on-select="false" data-placeholder="Select option"
                                    data-dropdown-parent="#kt_menu_select2" data-allow-clear="true"
                                    data-dt-col-name="task_status" id="filter-status">
                                    <option></option>
                                    @foreach (read_status() as $key => $item)
                                    <option value="{{$key}}">{{$item}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="mb-10">
                            <!--begin::Label-->
                            <label class="form-label fw-semibold">Type:</label>
                            <!--end::Label-->

                            <!--begin::Options-->
                            <div class="d-flex">
                                <!--begin::Options-->
                                <label class="form-check form-check-sm form-check-custom form-check-solid me-5">
                                    <input class="form-check-input" type="checkbox" value="Group" name="type"
                                        id="filter-type-group" data-dt-col-name="type" />
                                    <span class="form-check-label">
                                        Group
                                    </span>
                                </label>
                                <!--end::Options-->

                                <!--begin::Options-->
                                <label class="form-check form-check-sm form-check-custom form-check-solid">
                                    <input class="form-check-input" type="checkbox" value="Single" name="type"
                                        id="filter-type-single" data-dt-col-name="type" />
                                    <span class="form-check-label">
                                        Single
                                    </span>
                                </label>
                                <!--end::Options-->
                            </div>
                            <!--end::Options-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Input group-->
                        <div class="mb-10">
                            <!--begin::Label-->
                            <label class="form-label fw-semibold">Division :</label>
                            <!--end::Label-->

                            <!--begin::Input-->
                            <div>
                                <select class="form-select form-select-solid" multiple data-kt-select2="true" multiple
                                    data-close-on-select="false" data-placeholder="Select divisi"
                                    data-dropdown-parent="#kt_menu_select2" data-allow-clear="true"
                                    data-dt-col-name="division" id="filter-division">
                                    <option></option>
                                    @foreach ($divisions as $division)
                                    <option value="{{$division->id}}">{{$division->nama_divisi}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!--end::Input-->
                        </div>
                        <!--end::Input group-->

                        <!--begin::Actions-->
                        <div class="d-flex justify-content-end">
                            <button type="reset" class="btn btn-sm btn-light btn-active-light-primary me-2"
                                data-kt-menu-dismiss="true">Reset</button>

                            <button type="submit" class="btn btn-sm btn-primary" data-kt-menu-dismiss="true"
                                id="apply-filter">Apply</button>
                        </div>
                        <!--end::Actions-->
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Menu dropdown-->

                <!--begin::Add customer-->
                <!--begin::Menu-->
                <div class="me-0">
                    <button class="btn btn-icon btn-bg-light btn-active-color-primary d-flex align-items-center"
                        style="height: 100%" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                        <i class="ki-solid ki-dots-vertical fs-1"></i>
                    </button>
                    <!--begin::Menu 3-->
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-semibold w-200px py-3"
                        data-kt-menu="true">
                        <!--begin::Heading-->
                        <div class="menu-item px-3">
                            <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Quick Actions</div>
                        </div>
                        <!--end::Heading-->
                        <!--begin::Menu item-->
                        <div class="menu-item px-3">
                            <a href="{{ route('tasks.create') }}" class="menu-link px-3">Create Task</a>
                        </div>
                        <!--end::Menu item-->
                        <!--begin::Menu item-->
                        <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-end">
                            <a href="#" class="menu-link px-3" id="kt_datatable_example_export_menu">
                                <span class="menu-title">Exports</span>
                                <span class="menu-arrow"></span>
                            </a>
                            <!--begin::Menu sub-->
                            <div class="menu-sub menu-sub-dropdown w-175px py-4">
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3" data-kt-export="excel">
                                        Export as Excel
                                    </a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3" data-kt-export="csv">
                                        Export as CSV
                                    </a>
                                </div>
                                <!--end::Menu item-->
                                <!--begin::Menu item-->
                                <div class="menu-item px-3">
                                    <a href="#" class="menu-link px-3" data-kt-export="pdf">
                                        Export as PDF
                                    </a>
                                </div>
                                <!--end::Menu item-->
                            </div>
                            <!--end::Menu sub-->
                            <!--begin::Hide default export buttons-->
                            <div id="kt_datatable_example_buttons" class="d-none"></div>
                            <!--end::Hide default export buttons-->
                        </div>
                        <!--end::Menu item-->
                    </div>
                    <!--end::Menu 3-->
                </div>
                <!--end::Menu-->
                <!--end::Add customer-->
            </div>
            <!--end::Toolbar-->

            <!--begin::Group actions-->
            <div class="d-flex justify-content-end align-items-center d-none" data-kt-docs-table-toolbar="selected">
                <div class="fw-bold me-5">
                    <span class="me-2" data-kt-docs-table-select="selected_count"></span> Selected
                </div>

                <button type="button" class="btn btn-danger" data-bs-toggle="tooltip" title="Coming Soon">
                    Selection Action
                </button>
            </div>
            <!--end::Group actions-->
        </div>
        <!--end::Header2-->
        <!--begin::Body-->
        <div class="card-body py-3">
            @include('tasks.includes.table')
        </div>
        <!--begin::Body-->
    </div>
    <!--end::Tables Widget 9-->
</div>
<!--end::Row-->
@endsection

@push('scripts')
<script src="{{ asset('js/datas/tasks/adminTaskColumns.js') }}"></script>
<script src="{{ asset('js/customDt.js') }}"></script>
<script>
    $(document).ready(function() {
        var dataFilter = [];
        var dataArrayFilter = [{
            index: 10,
            value: [1]
        }];

        const dt = new customDt(
        {
            columns: columns,
            ajax: "{{ route('tasks.dt') }}",
        },
        {
            isSearch: true,
            column: 1,
        },
        dataFilter,
        {
            title: 'Tasks Report'
        },
        );

        dt.init();

        $('#filter-status').on('change', function(){
            var index = getIndexByValue(columns, 'name', $(this).attr('data-dt-col-name'));
            addToDataFilter(dt, dataFilter, index, $(this).val());
            // console.log("dataFilter")
            // console.log(dataFilter)
        });
        $('#filter-type-group, #filter-type-single').on('change', function() {
            $('.form-check-input').not(this).prop('checked', false);
            var index = getIndexByValue(columns, 'name', $(this).attr('data-dt-col-name'));
            addToDataFilter(dt, dataFilter, index, $(this).val());
            // console.log("dataFilter");
            // console.log(dataFilter);
        });

        $('#apply-filter').on('click', function(){
            // Reinitialize DataTable with updated filters
            dt.updateFilters(dataFilter);
            dt.reloadData(); // Reload data after applying filters
            console.log(dataFilter);
        });
    });

    function getIndexByValue(array, property, value) {
        return array.findIndex(obj => obj[property] === value);
    }

    function addToDataFilter(datatable, dataFilter, index, value){
        // Check if the index already exists in dataFilter
        var existingIndex = dataFilter.findIndex(function(item) {
            return item.index === index;
        });

        // If index already exists, update the value, otherwise push a new object
        if(existingIndex !== -1) {
            dataFilter[existingIndex].value = value;
        } else {
            dataFilter.push({
                index: index,
                value: value
            });
        }
    }
</script>
@endpush