  <!--begin::Input group-->
  <div class="mb-10">
    <!--begin::Label-->
    <label class="required form-label">Title</label>
    <!--end::Label-->
    <!--begin::Input-->
    <input type="text" name="task_title" id="task_title" class="form-control mb-2" placeholder="Task title" value="" />
    <!--end::Input-->
    <!--begin::Description-->
    <span class="text-danger form-error fs-7" id="title"></span>
    <!--end::Description-->
  </div>
  <!--end::Input group-->

  <!--begin::Input group-->
  <div class="mb-10">
    <!--begin::Label-->
    <label class="required form-label">Details</label>
    <!--end::Label-->
    <!--begin::Editor-->
    <div id="editor" name="task_description" class="min-h-200px mb-2"></div>
    <!--end::Editor-->
    <!--begin::Description-->
    <span class="text-danger form-error fs-7" id="description"></span>
    <!--end::Description-->
  </div>
  <!--end::Input group-->

  <!--begin::Input group-->
  <div class="row g-9 mb-10">
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
      <label class="required fs-6 fw-semibold mb-2">Date Estimate</label>
      <!--begin::Input-->
      <div class="position-relative d-flex align-items-center">
        <!--begin::Icon-->
        <i class="ki-outline ki-calendar-8 fs-2 position-absolute mx-4"></i>
        <!--end::Icon-->
        <!--begin::Datepicker-->
        <input class="form-control ps-12" id="datepicker" placeholder="Select a date" name="due_date" />
        <!--end::Datepicker-->
      </div>
      <!--end::Input-->
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
      <label class="required fs-6 fw-semibold mb-2">Priority</label>
      <select class="form-select" data-control="select2" data-hide-search="true" data-placeholder="Select a Team Member" name="target_assign">
        <option value="">Select priority...</option>
        <option value="0">Low</option>
        <option value="1">Medium</option>
        <option value="2">High</option>
      </select>
    </div>
    <!--end::Col-->
  </div>
  <!--end::Input group-->

  <!--begin::Input group-->
  <div class="row g-9 mb-10">
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
      <label class="required fs-6 fw-semibold mb-2">Divisi</label>
      <select name="divisi" aria-label="Select a divisi" data-control="select2" class="form-select">
        <option value="next">Within the next</option>
        <option value="last">Within the last</option>
        <option value="between">Between</option>
        <option value="on">On</option>
      </select>
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
      <label class="required fs-6 fw-semibold mb-2">Client</label>
      <select name="client" aria-label="Select a client" data-control="select2" class="form-select">
        <option value="next">Within the next</option>
        <option value="last">Within the last</option>
        <option value="between">Between</option>
        <option value="on">On</option>
      </select>
    </div>
    <!--end::Col-->
  </div>
  <!--end::Input group-->