      <!--begin::Table-->
      <table class="table table-row-dashed table-row-gray-300 align-middle gs-0 gy-4" id="dataTable">
        <!--begin::Table head-->
        <thead>
          <tr class="fw-bold text-muted">
            <th class="min-w-70px">#</th>
            <th class="">Task</th>
            <th class="min-w-150px">Company</th>
            <!-- <th class="min-w-150px">Client</th> -->
            <!-- <th class="min-w-150px">Publisher</th> -->
            <!-- <th class="min-w-100px">Input</th> -->
            <th class="min-w-200px">Team</th>
            <th class="min-w-100px">PIC</th>
            <th class="min-w-100px">T</th>
            <th class="min-w-100px">Start</th>
            <th class="min-w-100px">End</th>
            <th class="min-w-100px">R</th>
            <!-- <th class="min-w-200px">Detail</th> -->
            <th class="min-w-50px">Type</th>
            <th class="min-w-100px">Status</th>
            <!-- <th class="min-w-100px text-end">Actions</th> -->
          </tr>
        </thead>
        <!--end::Table head-->
        <!--begin::Table body-->
        <tbody>
        </tbody>
        <!--end::Table body-->
      </table>
      <!--end::Table-->