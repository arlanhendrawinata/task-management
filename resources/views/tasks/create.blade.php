@extends('layouts.app')
@section('title', 'Create New Tasks')
@section('content')
<!--begin::Row-->
<div class="row g-5 g-xl-8">
  <!--begin::Tables Widget 9-->
  <form id="formaze">
    <div class="card">
      <!--begin::Header-->
      <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
          <span class="card-label fw-bold fs-3 mb-1">Create Task</span>
          {{-- <span class="text-muted mt-1 fw-semibold fs-7">Over 500 members</span> --}}
        </h3>
      </div>
      <!--end::Header-->
      <!--begin::Body-->
      <div class="card-body py-3">
        @include('tasks.includes.create_form')
      </div>
      <!--begin::Body-->
    </div>
    <!--end::Tables Widget 9-->
    <!--begin::Actions-->
    <div class="d-flex justify-content-end">
      <button type="reset" class="btn btn-sm btn-light fw-bold btn-active-light-primary me-2" data-kt-search-element="preferences-dismiss">Cancel</button>
      <button type="submit" class="btn btn-sm fw-bold btn-primary">Save Changes</button>
    </div>
    <!--end::Actions-->
  </form>
</div>
<!--end::Row-->
@endsection
@push('scripts')
<script>
      const datepicker = document.getElementById('datepicker');
      console.log(datepicker)
      flatpickr(datepicker, {enableTime:!0,dateFormat:"Y-m-d H:i"})
    </script>

<script>
  $(document).ready(function() {
    $('form').submit(function(e) {
      e.preventDefault();
      $('.form-error').text('')
      var formData = {
        title: $('#task_title').val(),
        description: quill.root.innerHTML,
        _token: '{{ csrf_token() }}'
      }
      $.ajax({
        type: 'POST',
        url: '{{ route("tasks.store") }}',
        data: formData,
      }).done(function(res) {
        console.log(res)
      }).fail(function(xhr, status, error) {
        // If you want to return the response in case of an error
        if (xhr.responseText) {
          var errors = JSON.parse(xhr.responseText).errors;
          console.log(errors);
          $.each(errors, function(key, value){
            $('#' + key).text(value[0]);
          });
        } else {
          console.error('Error:', error);
        }
      }).always(function() {
        // This block will always execute, regardless of the request's outcome
        console.log('Request completed.');
      });
    })
  });

  const container = document.getElementById('editor');
  const quill = new Quill(container, {
    modules: {
      toolbar: [
        [{
          header: [1, 2, 3, false]
        }],
        ['bold', 'italic', 'underline'],
      ]
    },
    placeholder: 'Type your text here...',
    theme: 'snow' // or 'bubble'
  });
</script>
@endpush